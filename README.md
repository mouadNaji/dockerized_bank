# notes

# Feature 1

Real time updates on crypto currencies with Celery, Redis and Django Channels

Start broker:

```
$ docker run -d -p 6379:6379 redis
```

Run celery beat

```
$ celery -A bank_project beat -l INFO
```

In a new terminal window, run celery worker

```
$ celery -A bank_project worker -l INFO
```
