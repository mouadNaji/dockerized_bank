from django.contrib import admin

from .models import Costumer, Account, Ledger, SupportTicket

# Register your models here.

admin.site.register(Costumer)
admin.site.register(Account)
admin.site.register(Ledger)
admin.site.register(SupportTicket)
