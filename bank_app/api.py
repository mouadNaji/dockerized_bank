from rest_framework import generics
from .models import Costumer, Ledger, Account
from .serializers import Costumers, Ledgers, Accounts
from .permissions import IsAuthorOrReadOnly


class CostumerList(generics.ListAPIView):
    queryset = Costumer.objects.all()
    serializer_class = Costumers


class CostumerDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Costumer.objects.all()
    serializer_class = Costumers


class LedgerList(generics.ListCreateAPIView):
    queryset = Ledger.objects.all()
    serializer_class = Ledgers


class LedgerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ledger.objects.all()
    serializer_class = Ledgers


class AccountList(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = Accounts


class AccountDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = Accounts
