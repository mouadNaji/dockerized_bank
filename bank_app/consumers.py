import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer
from .models import Costumer, SupportTicket


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        pk = self.scope["user"].id
        user_name = self.scope["user"].username
        support_ticket = SupportTicket.objects.get(userid=pk, username=user_name)
        roomUUID = support_ticket.uuid.hex
        try:
            employee = Costumer.objects.get(user=pk, is_employee = True)
        except Costumer.DoesNotExist:
            employee = None
        
       # if supportticket uuid is the same as url OR admin, join room
        if(support_ticket.uuid.hex == self.scope['url_route']['kwargs']['room_name'] or employee):
            self.room_name = self.scope['url_route']['kwargs']['room_name']
            self.room_group_name = 'chat_%s' % self.room_name
            
            # Join room group
            async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
            )
        
            self.accept()
        
            # Send "has joined the room" to all rooms besides "EmployeeNotification" in base.html
            if self.room_name != "employeeNotification":
                async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': "has joined the room",
                    'username': user_name,
                }
            )

            # send the current roomUUID to "EmployeNotification" whenever a user joins the correct room
            if(support_ticket.uuid.hex == self.scope['url_route']['kwargs']['room_name'] and not employee):
                employeeSupport = 'chat_%s' % "employeeNotification"
                async_to_sync(self.channel_layer.group_send)(
                    employeeSupport,
                    {
                        'type': 'chat_message',
                        'message': roomUUID,
                        'username': user_name,
                    }
                )
        else:
            self.close()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

        # Send "has left the room" to all rooms besides "EmployeeNotification" in base.html
        if self.room_name != "employeeNotification":
            user_name = self.scope["user"].username
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': "has left the room",
                    'username': user_name,
                }
            )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        username = text_data_json['username']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'username': username,
            }
        )

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        username = event['username']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message,
            'username': username,
        }))



class CoinsConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add('coins', self.channel_name)
        await self.accept()
        print('connected')

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard('coins', self.channel_name)
        print('disconnected')

    async def send_coins(self, event):
        text_message = event['text']
        await self.send(text_message)
        # message = 'hi'
        # await self.send(text_data=json.dumps({ 'message': message}))

