from django import forms
from .models import Costumer


class CostumerForm(forms.ModelForm):
    class Meta:
        model = Costumer
        exclude = ()


class AddCostumer(forms.Form):
    RANKS=[('Gold','Gold'),
    ('Silver','Silver'),('Basic','Basic')]
    phone = forms.CharField()
    rank = forms.ChoiceField(choices=RANKS, widget=forms.RadioSelect)
    is_employee = forms.BooleanField(required=False)



