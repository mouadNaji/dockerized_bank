from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives


def email_message(message_dict):
    """test"""
    contents = f"""
    This is a test email
    """
    print(message_dict['email'])
      
    send_mail(
       'test',
       contents,
       'webdev88888@gmail.com',
       [message_dict['email']],
       fail_silently=False,
    )

def email_statement(message_dict):
    """Send account statement to user"""
    statements = []
    activities = message_dict['activities']
    balance = message_dict['balance']
    for activity in activities:
        statements.append(str(activity))
    contents = """
       Transaction overview:
       {}

       Current account balance: {}$
    """.format("<br>".join(statements), balance)
   
    HTMLContent = """
       <h2>Transaction overview:</h2>
       <p style="font-weight:700">{}</p>

       <h3>Current account balance: {}$</h3>

       <p style="font-weight:700">Best regards</p>
       <p style="font-weight:700">Cold friday</p>
    """.format("<br />".join(statements), balance)



    print(contents)


# send_mail function is commented out due to ban from sendinblue.com
# and the function will therefore not send an actual email

    #send_mail(
    #   'Bank statements',
    #   contents,
    #   'webdev88888@gmail.com',
    #    [message_dict['user_email']],
    #    fail_silently=False
    #)

    msg = EmailMultiAlternatives('bank statement', contents,'webdev88888@gmail.com', [message_dict['user_email']])
    msg.attach_alternative(HTMLContent, "text/html")
    msg.send()
