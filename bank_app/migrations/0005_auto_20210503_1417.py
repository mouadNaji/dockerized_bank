# Generated by Django 3.1.6 on 2021-05-03 14:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0004_ledger_regnnr'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ledger',
            old_name='regnnr',
            new_name='regnr',
        ),
    ]
