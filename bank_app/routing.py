from django.urls import re_path, path

from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/bank/chat/(?P<room_name>\w+)/$',
            consumers.ChatConsumer.as_asgi()),
    path('ws/coins/', consumers.CoinsConsumer.as_asgi()),
]
