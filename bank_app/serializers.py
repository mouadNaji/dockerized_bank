from rest_framework import serializers
from .models import Costumer, Ledger, Account


class Costumers(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'user', 'phone', 'rank',)
        model = Costumer


class Ledgers(serializers.ModelSerializer):
    class Meta:
        fields = ('account', 'amount', 'text', 'timestamp',)
        model = Ledger


class Accounts(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'is_account',)
        model = Account
