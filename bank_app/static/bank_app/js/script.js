let trend = document.querySelectorAll(".trend");

trend.forEach(elm => {
  let text = elm.textContent;
  let float = parseFloat(text);
  if (float > 0) {
    elm.style.color = "green";
  } else {
    elm.style.color = "red";
  }
});
