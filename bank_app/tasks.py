import requests
import json

from celery import shared_task
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django_rq import job


@shared_task
def get_coins():
    url = "https://coinranking1.p.rapidapi.com/coins"
    headers = {
        'x-rapidapi-key': "d648778688msh6b6a68838125f03p105bdbjsnee44dc3f26d8",
        'x-rapidapi-host': "coinranking1.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers).json()
    coins = json.dumps(response['data']['coins'])

    channel_layer = get_channel_layer()

    # group_send is async – to use in sync code, apply async_to_sync

    async_to_sync(channel_layer.group_send)(
        'coins', {'type': 'send_coins', 'text': coins})
