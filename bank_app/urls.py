from django.urls import path, include
from . import views
from .api import CostumerDetail, CostumerList, LedgerDetail, LedgerList, AccountDetail, AccountList


app_name = 'bank_app'

urlpatterns = [
    path('', views.index, name="index"),
    path('add_user', views.add_user, name="add_user"),
    path('change_rank', views.change_rank, name="change_rank"),
    path('add_account', views.add_account, name="add_account"),
    path('transfer_money', views.transfer_money, name="transfer_money"),
    path('external_transfer', views.external_transfer, name="external_transfer"),
    path('crypto', views.crypto, name="crypto"),
    path('make_loan', views.make_loan, name="make_loan"),
    path('pay_loan', views.pay_loan, name="pay_loan"),
    path('<int:Costumer_id>/', views.account_detail, name="account_detail"),
    path('logout', views.logout, name="logout"),
    path('test_smtp', views.test_smtp, name="test_smtp"),
    path('sendmail/<int:account_id>/', views.sendmail, name="sendmail"),
    # api urls
    path('api/v1/Costumer/<int:pk>/', CostumerDetail.as_view()),
    path('api/v1/Costumer', CostumerList.as_view()),
    path('api/v1/Ledger/<int:pk>/', LedgerDetail.as_view()),
    path('api/v1/Ledger', LedgerList.as_view()),
    path('api/v1/Account/<int:pk>', AccountDetail.as_view()),
    path('api/v1/Account/', AccountList.as_view()),
    path('api/v1/rest-auth/', include('rest_auth.urls')),
    # channel urls
    path('chat/<str:room_name>/', views.room, name='room'),
    path('support_chat', views.support_chat, name="support_chat"),
]
