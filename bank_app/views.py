from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import requests
from .models import Costumer, Account, Ledger, SupportTicket
from .forms import AddCostumer
from .tasks import get_coins
from django.contrib.auth import authenticate, logout as dj_logout
import json
from uuid import uuid4
import django_rq
from django_rq import job
from . messaging import email_message
from . messaging import email_statement
from django.shortcuts import redirect
from django.core.paginator import EmptyPage, Paginator
from django.urls import resolve
from asgiref.sync import async_to_sync

# Create your views here


@login_required
def index(request):

    costumer_form = AddCostumer()
    users = User.objects.all()
    all_accounts = Account.objects.all()
    accounts = Account.objects.filter(user=request.user, is_account=True)
    loans = Account.objects.filter(user=request.user, is_account=False)

    # get costumer of logged in user, pass it to template
    costumer = Costumer.objects.get(user=request.user)

    # allcoins

    url = "https://coinranking1.p.rapidapi.com/coins"

    headers = {
        'x-rapidapi-key': "d648778688msh6b6a68838125f03p105bdbjsnee44dc3f26d8",
        'x-rapidapi-host': "coinranking1.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers)

    result = response.text
    coins = json.loads(result)
    coinsdata = coins['data']['coins']

    context = {
        'accounts': accounts,
        'loans': loans,
        'all_accounts': all_accounts,
        'users': users,
        'costumer': costumer,
        'costumer_form': costumer_form,
        'coinsdata': coinsdata,
    }

    return render(request, 'bank_app/index.html', context)


def add_user(request):

    costumer_form = AddCostumer()
    costumer = Costumer.objects.get(user=request.user)
    users = User.objects.all()

    paginatedUsers = Paginator(users, 2)  # Show 2 contacts per page.
    page_num = request.GET.get('page', 1)

    try:
        page = paginatedUsers.page(page_num)
    except EmptyPage:
        page = paginatedUsers(1)

    context = {
        'costumer_form': costumer_form,
        'costumer': costumer,
        'users': page,
    }

    if request.method == "POST":
        username = request.POST['username']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        password = request.POST['password']
        email = request.POST['email']
        phone = request.POST['phone']
        rank = request.POST['rank']

        def is_employee(request):
            if request.POST.get('is_employee'):
                return True
            else:
                return False

        user = User.objects.create_user(username, email, password)

        if user:

            user.first_name = firstname
            user.last_name = lastname
            user.save()

            costumer = Costumer()

            costumer.user = user
            costumer.phone = phone
            costumer.is_employee = is_employee(request)
            costumer.rank = rank
            costumer.save()

    return render(request, 'bank_app/employee.html', context)

    # except:
    # return HttpResponse('<h1>Username has already been used - try
    # again</h1>')


def change_rank(request):

    pk = request.POST['pk']
    rank = request.POST['rank']
    user = User.objects.get(pk=pk)
    costumer = Costumer.objects.get(user=user)
    costumer.rank = rank
    costumer.save()

    return HttpResponseRedirect(reverse('bank_app:index'))


def add_account(request):
    pk = request.POST['pk']
    account_name = request.POST['account']
    user = User.objects.get(pk=pk)
    account = Account(user=user, name=account_name)
    account.save()

    return HttpResponseRedirect(reverse('bank_app:index'))


def transfer_money(request):

    costumer = Costumer.objects.get(user=request.user)
    accounts = Account.objects.filter(user=request.user, is_account=True)
    context = {
        'accounts': accounts,
        'costumer': costumer
    }

    if request.method == 'POST':

        credit_id = request.POST['from_account']
        debit_id = request.POST['to_account']
        amount = request.POST['amount']
        regnr = request.POST['regnr']
        text = request.POST['text']

        # check if debit_id exists
        if Account.objects.filter(id=debit_id).exists():
            # check if debit_id is account (not a loan)
            if Account.objects.filter(id=debit_id, is_account=True):
                # check if balance is sufficient
                if Account.objects.get(
                        user=request.user,
                        id=credit_id).balance >= int(amount):

                    Ledger.transaction(
                        amount, text, debit_id, credit_id, regnr)
                    return HttpResponseRedirect(reverse('bank_app:transfer_money'))

                else:
                    return HttpResponse("<h1>Sorry, insufficient funds</h1>")

            else:
                return HttpResponse("<h1>Sorry, receiver account must be an account (not loan)</h1>")

        else:
            return HttpResponse("<h1>Sorry, receiver account doesn't exist</h1>")

    return render(request, 'bank_app/transfer.html', context)


def external_transfer(request):

    if request.method == 'POST':

        credit_id = request.POST['from_account']
        regnr = request.POST['to_regnr']
        debit_id = request.POST['to_account']
        amount = request.POST['amount']
        text = request.POST['text']

        # STEP 1: CHECK FUNDS

        if Account.objects.get(user=request.user, id=credit_id).balance < int(amount):
            return HttpResponse('<h1>Sorry, insufficient funds</h1>')

        # STEP 2: AUTHENTICATE

        payload = {'username': 'admin', 'password': 'admin'}
        r = requests.post(
            f'http://127.0.0.1:{regnr}/bank/api/v1/rest-auth/login/', data=payload)

        token = r.json()['key']
        print(f'Token {token}')

        # STEP 3: CHECK IF RECEIVER ACCOUNT EXISTS

        headers = {'Authorization': f'Token {token}'}
        r = requests.get(
            f'http://127.0.0.1:{regnr}/bank/api/v1/Account/{debit_id}', headers=headers)

        print(r.text)
        print(r.status_code)

        # STEP 4: POST TO REST API

        # Bank account belonging to Bank 8000 inside Bank 4000:
        # Account id: 2

        if r.status_code == 200:
            print('Account exists')

            uuid = uuid4()

            payload_1 = {'account': '2',
                         'regnr': f'{regnr}',
                         'amount': f'-{amount}',
                         'text': f'{text}',
                         'transaction_id': f'{uuid}'
                         }

            payload_2 = {'account': f'{debit_id}',
                         'regnr': f'{regnr}',
                         'amount': f'{amount}',
                         'text': f'{text}',
                         'transaction_id': f'{uuid}'}

            r1 = requests.post(
                f'http://127.0.0.1:{regnr}/bank/api/v1/Ledger', headers=headers, data=payload_1)
            r2 = requests.post(
                f'http://127.0.0.1:{regnr}/bank/api/v1/Ledger', headers=headers, data=payload_2)

            print(r1.status_code)
            print(r2.status_code)

        else:
            return HttpResponse('<h1>Account doesnt exist or external bank is down</h1>')

        # STEP 5: TRANSACTION INTERNALLY

        # Bank account belonging to Bank 4000 inside Bank 8000:
        # Account id: 2

        debit_id = 2
        Ledger.transaction(amount, text, debit_id, credit_id, regnr)

    return render(request, 'bank_app/transfer.html')


def make_loan(request):

    accounts = Account.objects.filter(user=request.user, is_account=True)
    loans = Account.objects.filter(user=request.user, is_account=False)
    costumer = Costumer.objects.get(user=request.user)

    context = {
        'accounts': accounts,
        'loans': loans,
        'costumer': costumer,
    }

    if request.method == 'POST':

        amount = request.POST['amount']
        text = request.POST['text']
        loan_name = request.POST['loan_name']
        regnr = 8000

        # get id of debit account
        account_id = request.POST['account_name']

        account = Account(user=request.user, name=loan_name, is_account=False)
        account.save()

        # get id of credit account
        loan_id = account.id

        # if the given account name exists and belongs to the user

        if Account.objects.filter(user=request.user, id=account_id).exists():
            Ledger.transaction(amount, text, account_id, loan_id, regnr)

    return render(request, 'bank_app/loan.html', context)


def pay_loan(request):

    credit_id = request.POST['from_account']
    debit_id = request.POST['to_account']
    amount = request.POST['amount']
    regnr = request.POST['regnr']
    text = request.POST['text']

    if Account.objects.get(id=debit_id, is_account=False):
        # print('This is a loan account!')
        if Account.objects.get(id=debit_id, user=request.user, is_account=False):
            #print('This loan account belongs to the user')
            #loan_account = Account.objects.get(id=debit_id)
            loan_balance = Account.objects.get(id=debit_id).balance
            loan_balance = abs(int(loan_balance))
            amount = int(amount)

            if Account.objects.get(id=credit_id, user=request.user).balance >= amount:
                #print('You have enough money!')
                if amount >= loan_balance:
                    amount = loan_balance
                    Ledger.transaction(
                        amount, text, debit_id, credit_id, regnr)
                    # loan_account.delete()

                else:
                    Ledger.transaction(
                        amount, text, debit_id, credit_id, regnr)

            else:
                return HttpResponse("<h1>Sorry, insufficient funds</h1>")
        else:
            return HttpResponse("<h1>Sorry, you can only pay off loans that belong to yourself</h1>")
    else:
        return HttpResponse("<h1>Sorry, could not find loan account</h1>")

    return HttpResponseRedirect(reverse('bank_app:make_loan'))


def account_detail(request, Costumer_id):

    try:
        user = User.objects.get(id=Costumer_id)
    except User.DoesNotExist:
        raise Http404("User does not exist")

    costumer = Costumer.objects.get(user=request.user)
    accounts = Account.objects.filter(user=user, is_account=True)

    # ledgers must be account instance

    ledgers = Ledger.objects.filter(
        account__in=accounts).order_by('-timestamp')

    context = {
        'user': user,
        'accounts': accounts,
        'ledgers': ledgers,
        'costumer': costumer
    }

    if costumer.is_employee or Costumer_id == request.user.id:
        return render(request, 'bank_app/account_detail.html', context)
    else:
        raise Http404("not employee")


def crypto(request):
    costumer = Costumer.objects.get(user=request.user)

    # delay() puts a callable in the queue

    url = "https://coinranking1.p.rapidapi.com/coins"

    headers = {
        'x-rapidapi-key': "d648778688msh6b6a68838125f03p105bdbjsnee44dc3f26d8",
        'x-rapidapi-host': "coinranking1.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers).json()

    coins = response['data']['coins']

    context = {
        'coinsdata': coins,
        'costumer': costumer,
    }

    # channel_layer = get_channel_layer()

    # group_send is async – to use in sync code, apply async_to_sync

    # async_to_sync(channel_layer.group_send)(
    #     'coins', {'type': 'send_coins', 'text': coins})

    return render(request, 'bank_app/crypto.html', context)


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('bank_app:index'))


def chat(request):
    return render(request, 'bank_app/chat.html')


def room(request, room_name):
    costumer = Costumer.objects.get(user=request.user)

    return render(request, 'bank_app/room.html', {
        'room_name': room_name,
        'costumer': costumer
    })


def support_chat(request):
    pk = request.user.id
    user = User.objects.get(pk=pk)
    name = request.user.username
    user_name = User.objects.get(username=name)
    uuid = uuid4().hex

    try:
        support_Ticket = SupportTicket.objects.get(
            userid=user, username=user_name)
        uuid = support_Ticket.uuid.hex
        return HttpResponseRedirect(reverse('bank_app:room', kwargs={'room_name': uuid}))
    except SupportTicket.DoesNotExist:
        support_Ticket = SupportTicket()
        support_Ticket.userid = user
        support_Ticket.username = user_name
        support_Ticket.uuid = uuid
        support_Ticket.save()
        return HttpResponseRedirect(reverse('bank_app:room', kwargs={'room_name': uuid}))


def sendmail(request, account_id):
    """View for send statement"""
    costumer = Costumer.objects.get(user=request.user)
    user_email = request.user.email
    activities = Ledger.objects.filter(
        account=account_id).order_by('-timestamp')
    account = Account.objects.get(id=account_id)
    balance = account.balance
    print(balance)
    print(user_email)
    print(activities)

    # Send email with account statements to user
    # rq_worker will pass on relevant information to messaing.py file
    django_rq.enqueue(email_statement, {
         'balance': balance,
         'user_email': user_email,
         'activities': activities,
    })

    return HttpResponseRedirect(reverse('bank_app:index'))


def test_smtp(request):
    django_rq.enqueue(email_message, {
        'email': 'mouadnaji@hotmail.com',
    })
    return HttpResponseRedirect(reverse('bank_app:index'))
