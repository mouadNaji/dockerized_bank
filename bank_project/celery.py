import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bank_project.settings')

app = Celery('bank_project')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    'get_coins_5s': {
        'task': 'bank_app.tasks.get_coins',
        'schedule': 5.0
    }
}

app.autodiscover_tasks()
