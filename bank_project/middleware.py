from django.core.exceptions import PermissionDenied
from django.conf import settings
import logging

class IPFilterMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        allowed_ip_addresses = settings.\
            IPFILTER_MIDDLEWARE['ALLOWED_IP_ADDRESSES']
        client_ip_address = request.META.get('REMOTE_ADDR')
        print(f'** client ip address: {client_ip_address}')

        if client_ip_address not in allowed_ip_addresses:
            raise PermissionDenied

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        response['X-IP-FILTER'] = 'IP FILTER BY KEA'

        return response



class CountRequestsMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        self.count_requests = 0
        self.count_exceptions = 0

    def __call__(self, request, *args, **kwargs):
        self.count_requests += 1
        logging.info(f"Handled {self.count_requests} requests so far")
        return self.get_response(request)

    def process_exception(self, request, exception):
        self.count_exceptions += 1
        logging.error(f"Encountered {self.count_exceptions} exceptions so far")


